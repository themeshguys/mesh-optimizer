#include "Logger.h"
#include <cassert>

std::string Logger::generationToken(int generationNo)
{
	return "#generationNo: " + std::to_string(generationNo);
}

std::string Logger::chromosomeToken(const std::vector<std::string>& chromosomeParts)
{
	return "Chromosome " + joinChromosomeParts(chromosomeParts);
}

std::string Logger::valueToken(double value)
{
	return "Value " + std::to_string(value);
}

std::string Logger::fittestChromosomeToken(const std::string& chromosomeString)
{

	return "Maximum/Minimum chromosome: " + chromosomeString;
}

std::string Logger::fittestValueToken(double value)
{
	return "Maximum/Minimum value: " + std::to_string(value);
}

std::string Logger::joinChromosomeParts(const std::vector<std::string>& chromosomeParts)
{
	std::string chromosomeJoinedString = "";
	std::size_t count = chromosomeParts.size();
	for (std::size_t index = 0; index < count; ++index)
	{
		chromosomeJoinedString += chromosomeParts[index];
		if (index < count - 1)
			chromosomeJoinedString += ' ';
	}
	return chromosomeJoinedString;
}

Logger::Logger(const std::string& filePath, std::function<bool(double, double)> comparator) :
	_outputFile(filePath),
	_comparator(comparator)
{
	_outputFile << kStartToken.data() << std::endl;
}

Logger::~Logger()
{
	_outputFile << kEndToken.data() << std::endl;
	_outputFile << fittestChromosomeToken(_fittestChromosomeString) << std::endl;
	_outputFile << fittestValueToken(_fittestValue) << std::endl;
}

void Logger::logGeneration(const std::vector<std::vector<std::string>>& chromosomeStrings, const std::vector<double>& values)
{
	assert(chromosomeStrings.size() == values.size());
	if(_generationNumber == 0)
	{
		_fittestValue = values[0];
		_fittestChromosomeString = joinChromosomeParts(chromosomeStrings[0]);
	}
	std::size_t count = values.size();
	_outputFile << generationToken(_generationNumber++) << std::endl;
	for(std::size_t index = 0; index < count; ++index)
	{
		_outputFile << chromosomeToken(chromosomeStrings[index]) << std::endl;
		_outputFile << valueToken(values[index]) << std::endl;

		if(_comparator(values[index], _fittestValue))
		{
			_fittestValue = values[index];
			_fittestChromosomeString = joinChromosomeParts(chromosomeStrings[index]);
		}
	}
}
