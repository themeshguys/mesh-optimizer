#include "Maths.h"
#include <algorithm>

void Math::normalizeVector(std::vector<double>& vector, double minimum, double maximum)
{
	std::size_t size = vector.size();
	const auto multiplier = 1 / (maximum - minimum);
	
	for (std::size_t index = 0; index < size; ++index)
		vector[index] = (vector[index] - minimum) * multiplier;
}

void Math::normalizeVector(std::vector<double>& vector)
{
	auto[min, max] = std::minmax_element(std::begin(vector), std::end(vector));
	normalizeVector(vector, *min, *max);
}
