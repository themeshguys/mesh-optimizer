#pragma once
#include <functional>

enum class ResultType
{
	Minimize,
	Maximize
};

using DoubleComparer = std::function<bool(double, double)>;

DoubleComparer getResultType(ResultType resultType)
{
	if (resultType == ResultType::Minimize)
	{
		return [](double a, double b) { return a < b; };
	}
	return [](double a, double b) { return a > b; };
}