#pragma once
#include "Exports.h"
#include <vector>

class OPTIMIZER_EXPORTS Math
{
public:
	// This function is used if you have a more efficient 
	// way to get the minimum and maximum values without
	// iterating the vector again to get them.
	static void normalizeVector(std::vector<double>& vector, double minimum, double maximum);

	static void normalizeVector(std::vector<double>& vector);
};