#pragma once

#include <cstddef>
#include <array>
#include <numeric>

#include <cassert>

#include "effolkronium/random.hpp"

#include "Chromosome.hpp"
#include "ByteUtilities.h"
#include "Maths.h"
#include "Logger.h"
#include "Interval.h"
#include "Comparer.h"

template <std::size_t GeneNumber, std::size_t ChromosomeNumber, typename Function, std::size_t VariableCount>
class Population
{
public:
	using Chromosome_T = Chromosome<GeneNumber * VariableCount>;
	using ChromosomePointer = std::shared_ptr<Chromosome_T>;

	template <typename IntervalIterator>
	Population(IntervalIterator intervalBegin, IntervalIterator intervalEnd, ResultType resultType, double crossoverThreshold = 0.1, double mutationThreshold = 0.00001,
		const std::string& logFile = "log.txt");

	template <typename IntervalIterator, typename ChromosomeIterator>
	Population(IntervalIterator intervalBegin, IntervalIterator intervalEnd,
		ChromosomeIterator chromosomeBegin, ChromosomeIterator chromosomeEnd, ResultType resultType, double crossoverThreshold = 0.1, double mutationThreshold = 0.00001, 
		const std::string& logFile = "log.txt");

	Population& operator++();
	Population operator++(int);

private:
	using ByteUtils = ByteUtilities<GeneNumber>;
	void nextGeneration();

	std::tuple<std::vector<double>, std::pair<double, double>, double> getFitnessResults();
	std::vector<double> getCumulativeProbabilities(const std::vector<double>& fitnessResults, double fitnessesSum);
	std::vector<ChromosomePointer> getSelectedChromosomes(const std::vector<double>& cumulativeProbabilities, double epsilon);

	bool areVariablesInRange(const std::vector<double>& variables) const;

	void logCurrentGeneration();


	void generateRandom();
	ChromosomePointer generateRandomChromosome() const;

	std::vector<double> unpackVariables(ChromosomePointer chromosome);
	std::vector<std::bitset<GeneNumber>> deinterlaceChromosome(ChromosomePointer chromosome);

	std::array<ChromosomePointer, ChromosomeNumber> _chromosomes;
	std::vector<Interval> _intervals;

	Function _function;

	const double _kCrossoverThreshold;
	const double _kMutationThreshold;

	Logger _logger;

	ResultType _resultType : 1;
};

template<std::size_t GeneNumber, std::size_t ChromosomeNumber, typename Function, std::size_t VariableCount>
inline bool Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::areVariablesInRange(const std::vector<double>& variables) const
{
	assert(variables.size() == _intervals.size());
	for (auto index = 0; index < variables.size(); ++index)
	{
		auto variable = variables[index];
		if (!(variable >= _intervals[index].lower && variable <= _intervals[index].upper))
			return false;
	}
	return true;
}

template <std::size_t GeneNumber, std::size_t ChromosomeNumber, class Function, std::size_t VariableCount>
void Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::nextGeneration()
{
	//Population<GeneNumber, ChromosomeNumber, Function, VariableCount> newPopulation(std::begin(_intervals), std::end(_intervals));
	auto[fitnessResults, minMax, fitnessesSum] = getFitnessResults();
	auto[minValue, maxValue] = minMax;

	Math::normalizeVector(fitnessResults, minValue, maxValue);

	fitnessesSum = (fitnessesSum - minValue * ChromosomeNumber) / (maxValue - minValue);

	auto cumulativeProbabilities = getCumulativeProbabilities(fitnessResults, fitnessesSum);
	static constexpr auto epsilon = std::numeric_limits<double>::epsilon();
	auto selectedChromosomes = getSelectedChromosomes(cumulativeProbabilities, epsilon);
	//assert(selectedChromosomes.size() != 0);

	std::vector<std::size_t> emptyIndexes;
	
	std::vector<ChromosomePointer> toBeCrossovered;
	for (auto index = 0; index < ChromosomeNumber; ++index)
	{
		auto randomValue = effolkronium::random_static::get(epsilon, 1.);
		if (randomValue < _kCrossoverThreshold)
		{
			toBeCrossovered.push_back(selectedChromosomes[index]);
			selectedChromosomes[index] = nullptr;
			emptyIndexes.push_back(index);
		}
	}

	if (toBeCrossovered.size() % 2 == 1)
	{
		selectedChromosomes[emptyIndexes.back()] = toBeCrossovered.back();
		emptyIndexes.pop_back();
		toBeCrossovered.pop_back();
	}

	static constexpr std::int8_t kCrossoverPairOffset{ 2 };
	static constexpr std::int8_t kCrossoverOffset{ 1 };
	for (auto index = 0; index < toBeCrossovered.size(); index += kCrossoverPairOffset)
	{
		auto randomValue = effolkronium::random_static::get<int>(0, GeneNumber - 1);
		try
		{
			auto pair = Chromosome_T::crossover(*toBeCrossovered[index], *toBeCrossovered[index + kCrossoverOffset], randomValue);

			auto firstPointer = std::make_shared<Chromosome_T>(pair.first.toString());
			if (areVariablesInRange(unpackVariables(firstPointer)))
			{
				toBeCrossovered[index] = firstPointer;
			}

			auto secondPointer = std::make_shared<Chromosome_T>(pair.second.toString());
			if (areVariablesInRange(unpackVariables(secondPointer)))
			{
				toBeCrossovered[index + kCrossoverOffset] = secondPointer;
			}
			
			selectedChromosomes[emptyIndexes[index]] = toBeCrossovered[index];
			selectedChromosomes[emptyIndexes[index + kCrossoverOffset]] = toBeCrossovered[index + kCrossoverOffset];
		}
		catch (const std::out_of_range&)
		{}
	}

	for (auto& chromosome : toBeCrossovered)
	{
		for (auto index = 0; index < GeneNumber; ++index)
		{
			auto randomValue = effolkronium::random_static::get(epsilon, 1.);
			if (randomValue < _kMutationThreshold)
			{
				chromosome->mutate(index);
				if (!areVariablesInRange(unpackVariables(chromosome)))
					chromosome->mutate(index);
				//TODO: Check if still in interval.
			}
		}
	}

	//_chromosomes = std::array<ChromosomePointer, ChromosomeNumber>();

	for (auto index = 0; index < ChromosomeNumber; ++index)
	{
		_chromosomes[index] = selectedChromosomes[index];
	}

	logCurrentGeneration();
}

template<std::size_t GeneNumber, std::size_t ChromosomeNumber, typename Function, std::size_t VariableCount>
inline std::tuple<std::vector<double>, std::pair<double, double>, double> Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::getFitnessResults()
{
	std::vector<double> functionResults(ChromosomeNumber);
	double functionsSum{ 0 };
	double minValue{ 0 };
	double maxValue{ 0 };

	for (auto index = 0; index < ChromosomeNumber; ++index)
	{
		auto values = unpackVariables(_chromosomes[index]);
		assert(values[0] < 200);
		auto functionResult = _function(values);
		
		functionResult > maxValue ? maxValue = functionResult : maxValue = maxValue;
		functionResult < minValue ? minValue = functionResult : minValue = minValue;

		//assert(!isinf(functionResult) && !isnan(functionResult));

		functionResults[index] = functionResult;
		functionsSum += functionResult;
	}
	return { functionResults, {minValue, maxValue}, functionsSum };
}

template<std::size_t GeneNumber, std::size_t ChromosomeNumber, typename Function, std::size_t VariableCount>
inline std::vector<double> Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::getCumulativeProbabilities(const std::vector<double>& fitnessResults, double fitnessesSum)
{
	//compute cumulative probabilities in one step
	static constexpr float kBias = 0.01;
	double previousProbability{ 0 };
	std::vector<double> cumulativeProbabilities(ChromosomeNumber);
	const auto multiplier = 1 / (fitnessesSum + kBias * ChromosomeNumber);

	for (auto index = 0; index < ChromosomeNumber; ++index)
	{
		auto selectionProbability = (fitnessResults[index] + kBias) * multiplier;
		cumulativeProbabilities[index] = previousProbability + selectionProbability;
		previousProbability = cumulativeProbabilities[index];
	}

	return cumulativeProbabilities;
}

template<std::size_t GeneNumber, std::size_t ChromosomeNumber, typename Function, std::size_t VariableCount>
inline std::vector<std::shared_ptr<Chromosome<GeneNumber * VariableCount>>> Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::getSelectedChromosomes(const std::vector<double>& cumulativeProbabilities, double epsilon)
{
	std::vector<ChromosomePointer> selectedChromosomes;
	for (auto index = 0; index < ChromosomeNumber; ++index)
	{
		auto randomValue = effolkronium::random_static::get(epsilon, 1.);
		int newChromosomeIndex = 0;
		for (auto chromoIndex = 0; chromoIndex < ChromosomeNumber; ++chromoIndex)
		{
			if (randomValue <= cumulativeProbabilities[chromoIndex])
			{
				newChromosomeIndex = (_resultType == ResultType::Minimize) ? chromoIndex + 1 : chromoIndex;
				break;
			}
			
			if (randomValue > cumulativeProbabilities[chromoIndex]
				&& randomValue <= cumulativeProbabilities[chromoIndex + 1])
			{
				newChromosomeIndex = (_resultType == ResultType::Minimize) ? chromoIndex : chromoIndex + 1;
				break;
			}
		}
		selectedChromosomes.push_back(_chromosomes[newChromosomeIndex]);
	}
	return selectedChromosomes;
}


template <std::size_t GeneNumber, std::size_t ChromosomeNumber, typename Function, std::size_t VariableCount>
void Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::logCurrentGeneration()
{
	std::vector<std::vector<std::string>> chromosomeStrings(ChromosomeNumber);
	std::vector<double> values(ChromosomeNumber);
	for (auto index = 0; index < ChromosomeNumber; ++index)
	{
		chromosomeStrings[index].resize(VariableCount);
		auto bitsets = deinterlaceChromosome(_chromosomes[index]);
		std::vector<double> currentValues(VariableCount);
		for (auto variableIndex = 0; variableIndex < VariableCount; ++variableIndex)
		{
			chromosomeStrings[index][variableIndex] = bitsets[variableIndex].to_string();
			currentValues[variableIndex] = ByteUtils::realFromBits(bitsets[variableIndex], _intervals[variableIndex]);
		}
		values[index] = _function(currentValues);
	}
	_logger.logGeneration(chromosomeStrings, values);
}

template <std::size_t GeneNumber, std::size_t ChromosomeNumber, class Function, std::size_t VariableCount>
void Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::generateRandom()
{
	for (auto index = 0; index < ChromosomeNumber; ++index)
	{
		_chromosomes[index] = generateRandomChromosome();
	}
	logCurrentGeneration();
}

template <std::size_t GeneNumber, std::size_t ChromosomeNumber, class Function, std::size_t VariableCount>
typename Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::ChromosomePointer Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::
generateRandomChromosome() const
{
	std::bitset<GeneNumber> variableBitset;

	std::string chromosomeString(GeneNumber * VariableCount, '0');

	for (auto variableIndex = 0; variableIndex < VariableCount; ++variableIndex)
	{
		const auto& interval = _intervals[variableIndex];

		for (auto bitIndex = 0; bitIndex < variableBitset.size(); ++bitIndex)
		{
			variableBitset[bitIndex] = effolkronium::random_static::get(0, 1);
		}

		auto longValue = variableBitset.to_ullong();
		auto doubleValue = interval.lower + longValue *
			(interval.upper - interval.lower) / ((1 << GeneNumber) - 1);

		assert(doubleValue >= interval.lower && doubleValue <= interval.upper);

		auto bits = ByteUtils::bitsFromReal(doubleValue, interval);
		for (auto bitIndex = 0; bitIndex < bits.size(); ++bitIndex)
		{
			chromosomeString[bitIndex * VariableCount + variableIndex] = bits[bitIndex] + '0';
		}
	}
	return std::make_shared<Chromosome_T>(chromosomeString);
}

template <std::size_t GeneNumber, std::size_t ChromosomeNumber, class Function, std::size_t VariableCount>
std::vector<std::bitset<GeneNumber>> Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::deinterlaceChromosome(
	ChromosomePointer chromosome)
{
	std::vector<std::bitset<GeneNumber>> chromosomeBits;
	auto chromosomeString = chromosome->toString();
	for (auto variableIndex = 0; variableIndex < VariableCount; ++variableIndex)
	{
		std::bitset<GeneNumber> bits;
		for (auto bitIndex = 0; bitIndex < bits.size(); ++bitIndex)
		{
			bits[bitIndex] = chromosomeString[bitIndex * VariableCount + variableIndex] - '0';
		}
		chromosomeBits.push_back(bits);
	}
	return chromosomeBits;
}

template<std::size_t GeneNumber, std::size_t ChromosomeNumber, typename Function, std::size_t VariableCount>
inline std::vector<double> Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::unpackVariables(ChromosomePointer chromosome)
{
	std::vector<double> values(VariableCount);
	auto bitsets = deinterlaceChromosome(chromosome);
	for (auto variableIndex = 0; variableIndex < VariableCount; ++variableIndex)
		values[variableIndex] = ByteUtils::realFromBits(bitsets[variableIndex], _intervals[variableIndex]);
	return values;
}

template <std::size_t GeneNumber, std::size_t ChromosomeNumber, class Function, std::size_t VariableCount>
Population<GeneNumber, ChromosomeNumber, Function, VariableCount>& Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::operator++()
{
	nextGeneration();
	return *this;
}

template <std::size_t GeneNumber, std::size_t ChromosomeNumber, class Function, std::size_t VariableCount>
Population<GeneNumber, ChromosomeNumber, Function, VariableCount> Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::operator++(int)
{
	Population other = *this;
	operator++();
	return other;
}

template<std::size_t GeneNumber, std::size_t ChromosomeNumber, typename Function, std::size_t VariableCount>
template<typename IntervalIterator>
inline Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::Population(IntervalIterator intervalBegin, IntervalIterator intervalEnd, ResultType resultType, double crossoverThreshold, double mutationThreshold, const std::string& logFile)
	: _intervals(intervalBegin, intervalEnd),
	_kCrossoverThreshold(crossoverThreshold),
	_kMutationThreshold(mutationThreshold),
	_resultType(resultType),
	_logger(logFile, getResultType(resultType))
{
	assert(std::distance(intervalBegin, intervalEnd) == VariableCount);
	generateRandom();
}

template<std::size_t GeneNumber, std::size_t ChromosomeNumber, typename Function, std::size_t VariableCount>
template<typename IntervalIterator, typename ChromosomeIterator>
inline Population<GeneNumber, ChromosomeNumber, Function, VariableCount>::Population(IntervalIterator intervalBegin, IntervalIterator intervalEnd, ChromosomeIterator chromosomeBegin, ChromosomeIterator chromosomeEnd, ResultType resultType, double crossoverThreshold, double mutationThreshold, const std::string& logFile)
	: _intervals(intervalBegin, intervalEnd),
	_chromosomes(chromosomeBegin, chromosomeEnd),
	_kCrossoverThreshold(crossoverThreshold),
	_kMutationThreshold(mutationThreshold),
	_resultType(resultType),
	_logger(logFile, getResultType(resultType))
{
	assert(std::distance(intervalBegin, intervalEnd) == VariableCount);
}
