#pragma once

#include <cstddef>
#include <bitset>
#include <string_view>

#include "Exports.h"

template <std::size_t N>
class Chromosome
{
public:
	static std::pair<Chromosome, Chromosome> crossover(const Chromosome& lhs, const Chromosome& rhs, int divisionIndex);
	static constexpr std::int8_t kZeroIndex{ 0 };
public:
	explicit Chromosome(const std::string& genes);

	std::string toString() const;

	void mutate(int geneIndex);

private:
	static void checkIndexBounds(int index, std::string_view message = "Index out of range");

	std::bitset<N> _genes;
};

template<std::size_t N>
inline std::pair<Chromosome<N>, Chromosome<N>> Chromosome<N>::crossover(const Chromosome& lhs, const Chromosome& rhs, int divisionIndex)
{
	static constexpr std::int8_t kDivisonOffset{ 1 };

	checkIndexBounds(divisionIndex, "Division index out of range");
	auto splitIndex = divisionIndex + kDivisonOffset;

	auto lhsString = lhs.toString();
	auto rhsString = rhs.toString();

	Chromosome<N> first(lhsString.substr(kZeroIndex, splitIndex) + rhsString.substr(splitIndex, N - splitIndex));
	Chromosome<N> second(rhsString.substr(kZeroIndex, splitIndex) + lhsString.substr(splitIndex, N - splitIndex));

	return { first, second };
}

template<std::size_t N>
inline Chromosome<N>::Chromosome(const std::string& genes)
	: _genes(genes)
{
	static_assert(N > 0, "Genes number must be positive.");
}

template<std::size_t N>
inline void Chromosome<N>::mutate(int geneIndex)
{
	checkIndexBounds(geneIndex, "Gene mutation index out of range.");
	_genes.flip(N - geneIndex - 1);
}

template<std::size_t N>
inline std::string Chromosome<N>::toString() const
{
	return _genes.to_string();
}

template<std::size_t N>
inline void Chromosome<N>::checkIndexBounds(int index, std::string_view message)
{
	if (index < kZeroIndex || index >= N)
		throw std::out_of_range(message.data());
}


