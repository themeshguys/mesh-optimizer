#pragma once

struct Interval
{
	double lower;
	double upper;

	Interval(double lower, double upper) :
		lower(lower),
		upper(upper)
	{}
};
