#pragma once
#include "Exports.h"
#include <string>
#include <vector>
#include <fstream>
#include <functional>

class OPTIMIZER_EXPORTS Logger
{
public:
	Logger(const std::string& filePath, std::function<bool(double, double)> comparator);
	~Logger();

	void logGeneration(const std::vector<std::vector<std::string>>& chromosomeStrings, const std::vector<double>& values);

private:
	int _generationNumber = 0;
	std::ofstream _outputFile;

	std::function<bool(double, double)> _comparator;

	double _fittestValue = 0;
	std::string _fittestChromosomeString;

	static constexpr std::string_view kStartToken = "#start";
	static constexpr std::string_view kEndToken = "#end";
	static std::string generationToken(int generationNo);
	static std::string chromosomeToken(const std::vector<std::string>& chromosomeParts);
	static std::string valueToken(double value);
	static std::string fittestChromosomeToken(const std::string& chromosomeString);
	static std::string fittestValueToken(double value);
	static std::string joinChromosomeParts(const std::vector<std::string>& chromosomeParts);
};

