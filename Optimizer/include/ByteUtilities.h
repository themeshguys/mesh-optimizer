#pragma once
#include <bitset>
#include "Interval.h"

template <std::size_t BitCount>
class ByteUtilities
{
public:
	static double realFromBits(const std::bitset<BitCount>& bitset, const Interval& interval);
	static std::bitset<BitCount> bitsFromReal(double value, const Interval& interval);

};

template <std::size_t BitCount>
double ByteUtilities<BitCount>::realFromBits(const std::bitset<BitCount>& bitset, const Interval& interval)
{
	auto longValue = bitset.to_ullong();
	return interval.lower + longValue *
		(interval.upper - interval.lower) / ((1 << BitCount) - 1);
}

template <std::size_t BitCount>
std::bitset<BitCount> ByteUtilities<BitCount>::bitsFromReal(double value, const Interval& interval)
{
	return std::bitset<BitCount>((value - interval.lower) * ((1 << BitCount) - 1) / (interval.upper - interval.lower));
}
