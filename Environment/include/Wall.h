#pragma once
#include "Body.h"

class Wall : public Body
{
public:
	Wall(const Domain::Size& size, double density, bool isFixed = true);
};