#pragma once

#include "chrono/physics/ChBodyEasy.h"
#include "chrono/assets/ChTexture.h"

#include "Domain.h"

#include <memory>

class Body
{
public:
	Body(const Domain::Size& size, double density, bool collide = true, bool visualAsset = true);

	void attach(std::shared_ptr<chrono::ChTexture> texture);

	void setPosition(chrono::ChVector<> position);

	std::shared_ptr<chrono::ChBodyEasyBox> getBody();
protected:
	std::shared_ptr<chrono::ChBodyEasyBox> _body;
};