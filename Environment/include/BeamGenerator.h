#pragma once
#include <vector>
#include <core/ChVector.h>
#include <fea/ChMesh.h>

#include "Domain.h"

class BeamGenerator
{
public:
	static std::vector<chrono::ChVector<>> getCubePoints(const chrono::ChVector<>& translation = chrono::ChVector<>{ 0, 0, 0 }, double scale = 1.f);
	static std::shared_ptr<chrono::fea::ChMesh> generate(const Domain::Size& size, std::shared_ptr<chrono::fea::ChContinuumElastic> material, double cubeLength = 0.5f);
};
