#include "chrono/core/ChVector.h"

#include "Domain.h"

namespace Constants
{
	constexpr auto kWallSize = Domain::Size(10, 5, 0.2);
	constexpr auto kBoxSize = Domain::Size(0.1, 0.1, 0.1);

	const auto kWallPosition = chrono::ChVector<>(0, 0, 0);
}