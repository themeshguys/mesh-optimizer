#pragma once

#include <memory>

#include "chrono/fea/ChElementCableANCF.h"
#include "chrono/physics/ChBody.h"
#include "chrono/physics/ChSystemNSC.h"

class Cable
{
public:
	Cable(chrono::ChSystem& system, std::shared_ptr<chrono::fea::ChMesh> mesh, std::shared_ptr<chrono::fea::ChMesh> beam, std::shared_ptr<chrono::ChBody> box);

private:
	void addVisualizations(std::shared_ptr<chrono::fea::ChMesh> mesh);

	std::shared_ptr<chrono::fea::ChBeamSectionCable> _cable;
};