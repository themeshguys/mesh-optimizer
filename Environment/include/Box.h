#pragma once
#include "Body.h"

class Box : public Body
{
public:
	Box(const Domain::Size& size, double density, double mass = 0.0);
};

