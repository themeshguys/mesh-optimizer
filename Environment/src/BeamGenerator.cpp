#include "BeamGenerator.h"
#include <fea/ChNodeFEAxyz.h>
#include <fea/ChElementTetra_4.h>

std::vector<chrono::ChVector<>> BeamGenerator::getCubePoints(const chrono::ChVector<>& translation, double scale)
{
	auto points = std::vector<chrono::ChVector<>> {
		chrono::ChVector<>(0, 0, 0),
		chrono::ChVector<>(0, 1, 0),
		chrono::ChVector<>(1, 1, 0),
		chrono::ChVector<>(1, 0, 0),
		chrono::ChVector<>(0, 0, 1),
		chrono::ChVector<>(0, 1, 1),
		chrono::ChVector<>(1, 1, 1),
		chrono::ChVector<>(1, 0, 1)
	};

	for (auto& point : points)
		point = (point + translation) * scale;

	return points;
}

std::shared_ptr<chrono::fea::ChMesh> BeamGenerator::generate(const Domain::Size& size, std::shared_ptr<chrono::fea::ChContinuumElastic> material, double cubeLength)
{
	struct Hasher
	{
		std::size_t operator()(const chrono::ChVector<>& vec) const
		{
			return std::hash<std::string>{}(
				std::to_string(vec.x()) + ", " 
				+ std::to_string(vec.y()) + ", " 
				+ std::to_string(vec.z()));
		}
	};
	auto[xSize, ySize, zSize] = size;

	assert(xSize > 0);
	assert(ySize > 0);
	assert(zSize > 0);

	auto myMesh = std::make_shared<chrono::fea::ChMesh>();
	std::unordered_map<chrono::ChVector<>, std::shared_ptr<chrono::fea::ChNodeFEAxyz>, Hasher> nodes;

	for(int i = 0; i < xSize; ++i)
		for(int j = 0; j < ySize; ++j)
			for(int k = 0; k < zSize; ++k)
			{
				auto points = getCubePoints(chrono::ChVector<>(i, j, k), cubeLength);
				std::vector<std::shared_ptr<chrono::fea::ChNodeFEAxyz>> elements;
				for(auto& point : points)
				{
					auto it = nodes.find(point);
					std::shared_ptr<chrono::fea::ChNodeFEAxyz> node;
					if(it == nodes.end())
					{
						node = std::make_shared<chrono::fea::ChNodeFEAxyz>(point);
						nodes.insert({ point, node });
					}
					else
					{
						node = it->second;
					}
					elements.push_back(node);
				}
				std::vector<std::shared_ptr<chrono::fea::ChElementTetra_4>> tetras;

				for (int i = 0; i < 5; ++i)
					tetras.push_back(std::make_shared<chrono::fea::ChElementTetra_4>());

				tetras[0]->SetNodes(elements[3], elements[1], elements[0], elements[4]);
				tetras[1]->SetNodes(elements[3], elements[6], elements[7], elements[4]);
				tetras[2]->SetNodes(elements[1], elements[2], elements[3], elements[6]);
				tetras[3]->SetNodes(elements[1], elements[4], elements[5], elements[6]);
				tetras[4]->SetNodes(elements[1], elements[4], elements[3], elements[6]);

				for (auto& tetra : tetras)
				{
					tetra->SetMaterial(material);
					myMesh->AddElement(tetra);
				}
			}

	for (auto &[point, node] : nodes)
		myMesh->AddNode(node);

	return myMesh;
}
