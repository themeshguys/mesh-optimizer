#include "Body.h"

Body::Body(const Domain::Size & size, double density, bool collide, bool visualAsset)
{
	auto[x, y, z] = size;
	_body = std::make_shared<chrono::ChBodyEasyBox>(x, y, z, density, collide, visualAsset);
}

void Body::attach(std::shared_ptr<chrono::ChTexture> texture)
{
	_body->AddAsset(texture);
}

void Body::setPosition(chrono::ChVector<> position)
{
	_body->SetPos(position);
}

std::shared_ptr<chrono::ChBodyEasyBox> Body::getBody()
{
	return _body;
}
