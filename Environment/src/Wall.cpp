#include "Wall.h"

Wall::Wall(const Domain::Size & size, double density, bool isFixed)
	: Body(size, density)
{
	_body->SetBodyFixed(isFixed);
}
