#include "Box.h"

Box::Box(const Domain::Size & size, double density, double mass)
	: Body(size, density)
{
	_body->SetMass(mass);
}
