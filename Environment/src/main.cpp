//// =============================================================================
//// PROJECT CHRONO - http://projectchrono.org
////
//// Copyright (c) 2014 projectchrono.org
//// All rights reserved.
////
//// Use of this source code is governed by a BSD-style license that can be found
//// in the LICENSE file at the top level of the distribution and at
//// http://projectchrono.org/license-chrono.txt.
////
//// =============================================================================
//// Authors: Alessandro Tasora
//// =============================================================================
////
//// FEA visualization using Irrlicht
////
//// =============================================================================
//
//#include "chrono/physics/ChSystemNSC.h"
//#include "chrono/physics/ChBodyEasy.h"
//
//#include "chrono/solver/ChSolverMINRES.h"
//
//#include "chrono/fea/ChElementTetra_4.h"
//#include "chrono/fea/ChLinkPointFrame.h"
//#include "chrono/fea/ChLinkPointPoint.h"
//#include "chrono/fea/ChVisualizationFEAmesh.h"
//#include "chrono/fea/ChElementCableANCF.h"
//#include "chrono/fea/ChBuilderBeam.h"
//
//#include "chrono_irrlicht/ChIrrApp.h"
//
//#include "Wall.h"
//#include "Box.h"
//#include "BeamGenerator.h"
//#include "Cable.h"
//
//#include "Constants.h"
//
//using namespace chrono;
//using namespace chrono::fea;
//using namespace chrono::irrlicht;
//
//using namespace irr;
//
//void addBeamConstraintsToTheWall(ChSystem& system, std::shared_ptr<ChMesh> beam, Wall& wall)
//{
//	for (unsigned int inode = 0; inode < beam->GetNnodes(); ++inode) {
//		if (auto mnode = std::dynamic_pointer_cast<ChNodeFEAxyz>(beam->GetNode(inode))) {
//			if (mnode->GetPos().z() < 0.01) {
//				auto constraint = std::make_shared<ChLinkPointFrame>();
//				constraint->Initialize(mnode, wall.getBody());
//				system.Add(constraint);
//
//				// For example, attach small cube to show the constraint
//				auto mboxfloor = std::make_shared<ChBoxShape>();
//				mboxfloor->GetBoxGeometry().Size = ChVector<>(0.005);
//				constraint->AddAsset(mboxfloor);
//			}
//		}
//	}
//}
//
//double computeStrain(std::shared_ptr<ChElementBase> element)
//{
//	auto tetra = std::dynamic_pointer_cast<ChElementTetra_4>(element);
//	assert(tetra != null);
//	double e1 = 0, e2 = 0, e3 = 0;
//	tetra->GetStrain().ComputePrincipalStrains(e1, e2, e3);
//	return e1 + e2 + e3;
//}
//
//void printStrain(std::shared_ptr<ChElementBase> element, int index)
//{
//	auto strain = computeStrain(element);
//	GetLog() << "Tetrahedron index: " << index << "\n";
//	GetLog() << "Strain:" << strain << "\n";
//	GetLog() << "Node coordinates:" << "\n";
//	for (auto nodeIndex = 0; nodeIndex < element->GetNnodes(); ++nodeIndex)
//	{
//		auto pos = std::dynamic_pointer_cast<ChNodeFEAxyz>(element->GetNodeN(nodeIndex))->pos;
//		GetLog() << pos.x() << ", " << pos.y() << ", " << pos.z() << "\n";
//	}
//	GetLog() << "\n";
//}
//
//int main(int argc, char* argv[]) {
//	GetLog() << "Copyright (c) 2017 projectchrono.org\nChrono version: " << CHRONO_VERSION << "\n\n";
//
//	// Create a Chrono::Engine physical system
//	ChSystemNSC mySystem;
//
//	// Create the Irrlicht visualization (open the Irrlicht device,
//	// bind a simple user interface, etc. etc.)
//	ChIrrApp application(&mySystem, L"Irrlicht FEM visualization", core::dimension2d<u32>(1280, 720), false, true);
//
//	// Easy shortcuts to add camera, lights, logo and sky in Irrlicht scene:
//	application.AddTypicalLogo();
//	application.AddTypicalSky();
//	application.AddTypicalLights();
//	application.AddTypicalCamera(core::vector3df(0, (f32)0.0, -8));
//
//
//	// Create a material, that must be assigned to each element,
//	// and set its parameters
//	auto meshMaterial = std::make_shared<ChContinuumElastic>();
//	meshMaterial->Set_E(0.01e9);  // rubber 0.01e9, steel 200e9
//	meshMaterial->Set_v(0.3);
//	meshMaterial->Set_RayleighDampingK(0.001);
//	meshMaterial->Set_density(1000);
//
//
//	auto beam = BeamGenerator::generate(Domain::Size(4, 2, 16), meshMaterial, 0.25);
//	mySystem.Add(beam);
//
//	auto visualizeMesh = std::make_shared<ChVisualizationFEAmesh>(*(beam.get()));
//	visualizeMesh->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_NODE_SPEED_NORM);
//	visualizeMesh->SetColorscaleMinMax(0.0, 5.50);
//	visualizeMesh->SetShrinkElements(true, 0.85);
//	visualizeMesh->SetSmoothFaces(true);
//	beam->AddAsset(visualizeMesh);
//	//It seems that the beam's behaviour depends on its surface's material. Comment/uncomment the above line to observe the behaviour.
//
//	//texture
//	auto texture = std::make_shared<ChTexture>();
//	texture->SetTextureFilename(GetChronoDataFile("concrete.jpg"));
//
//	//wall
//	Wall wall(Domain::Size(Constants::kWallSize), 500);
//	wall.setPosition(Constants::kWallPosition);
//	wall.attach(texture);
//	mySystem.Add(wall.getBody());
//
//	//box
//	Box box(Domain::Size(Constants::kBoxSize), 10, 0);
//	box.setPosition((0, 0, 5));
//	box.attach(texture);
//	static const auto kOffset = ChVector<>(-0.4, -1.95, -0.1);
//	auto lastPoint = std::dynamic_pointer_cast<ChNodeFEAxyz>(beam->GetNodes().back());
//	box.getBody()->SetPos(lastPoint->GetPos() + kOffset);
//	mySystem.Add(box.getBody());
//
//	//cable
//	auto cableMesh = std::make_shared<ChMesh>();
//	Cable cable(mySystem, cableMesh, beam, box.getBody());
//	mySystem.Add(cableMesh);
//	
//	application.AssetBindAll();
//
//	// ==IMPORTANT!== Use this function for 'converting' into Irrlicht meshes the assets
//	// that you added to the bodies into 3D shapes, they can be visualized by Irrlicht!
//
//	application.AssetUpdateAll();
//
//	// Mark completion of system construction
//	mySystem.SetupInitial();
//
//
//	mySystem.SetTimestepperType(chrono::ChTimestepper::Type::EULER_IMPLICIT_LINEARIZED);
//
//	mySystem.SetSolverType(ChSolver::Type::MINRES);
//	mySystem.SetSolverWarmStarting(true);
//	mySystem.SetMaxItersSolverSpeed(40);
//	mySystem.SetTolForce(1e-10);
//
//	application.SetTimestep(0.001);
//
//	addBeamConstraintsToTheWall(mySystem, beam, wall);
//	
//
//	int frameNumber = 0;
//	while (application.GetDevice()->run()) {
//		application.BeginScene();
//		application.DrawAll();
//		application.DoStep();
//		application.EndScene();
//		if (frameNumber == 200)
//		{
//			auto elements = beam->GetElements();
//			int maxStrainElem = 0, minStrainElem = 0;
//			for (int elemIndex = 1; elemIndex < elements.size(); ++elemIndex)
//			{
//				auto strain = computeStrain(elements[elemIndex]);
//				if (strain > maxStrainElem)
//					maxStrainElem = elemIndex;
//				if (strain < minStrainElem)
//					minStrainElem = elemIndex;
//			}
//			GetLog() << "Minimum strain:\n";
//			printStrain(elements[minStrainElem], minStrainElem);
//			GetLog() << "Maximum strain:\n";
//			printStrain(elements[maxStrainElem], maxStrainElem);
//		}
//		++frameNumber;
//	}
//
//	return 0;
//}

#include "Population.hpp"
#include <iostream>

#include <cmath>

int main()
{
	struct Function
	{
		double operator()(std::vector<double> values)
		{
			auto x = values[0];
			return x * x + 6 * x + 1;
		}
	};

	using Population_T = Population<10, 30, Function, 1>;
	auto intervals = std::vector<Interval>{
		{-10., 10.}
	};

	Population_T population(std::begin(intervals), std::end(intervals), ResultType::Minimize, 0.15, 0.0005, "logs/min_2.txt");

	for (auto generation = 1; generation <= 100; ++generation)
	{
		++population;
	}
	return 0;
}