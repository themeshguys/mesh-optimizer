#include "../include/Cable.h"

#include "chrono/fea/ChBuilderBeam.h"
#include "chrono/fea/ChLinkPointFrame.h"
#include "chrono/fea/ChLinkPointPoint.h"
#include "chrono/fea/ChVisualizationFEAmesh.h"

using namespace chrono;
using namespace chrono::fea;

Cable::Cable(chrono::ChSystem & system, std::shared_ptr<chrono::fea::ChMesh> mesh, std::shared_ptr<chrono::fea::ChMesh> beam, std::shared_ptr<chrono::ChBody> box)
{
	//cable
	_cable = std::make_shared<chrono::fea::ChBeamSectionCable>();
	_cable->SetDiameter(0.035);
	_cable->SetYoungModulus(0.01e9);
	_cable->SetBeamRaleyghDamping(0.000);

	//create the beam
	ChBuilderBeamANCF builder;
	static constexpr int kFirstHangingPointIndex = 240;
	auto firstHangingPoint = std::dynamic_pointer_cast<ChNodeFEAxyz>(beam->GetNodes().at(kFirstHangingPointIndex));
	builder.BuildBeam(mesh,
		_cable,
		1,
		firstHangingPoint->GetPos(),
		box->GetPos());

	//tie the cable to the box
	auto boxConstraint = std::make_shared<ChLinkPointFrame>();
	boxConstraint->Initialize(builder.GetLastBeamNodes().back(), box);
	system.Add(boxConstraint);

	//tie the cable to the first point of the beam
	auto firstBeamConstraint = std::make_shared<ChLinkPointPoint>();
	firstBeamConstraint->Initialize(builder.GetLastBeamNodes().front(), firstHangingPoint);
	system.Add(firstBeamConstraint);

	//tie the cable to the second point of the beam
	static constexpr int kSecondHangingPointIndex = 238;
	auto secondHangingPoint = std::dynamic_pointer_cast<ChNodeFEAxyz>(beam->GetNodes().at(kSecondHangingPointIndex));
	auto secondBeamConstraint = std::make_shared<ChLinkPointPoint>();
	secondBeamConstraint->Initialize(builder.GetLastBeamNodes().front(), secondHangingPoint);
	system.Add(secondBeamConstraint);

	addVisualizations(mesh);
}

void Cable::addVisualizations(std::shared_ptr<chrono::fea::ChMesh> mesh)
{
	auto mvisualizebeamA = std::make_shared<ChVisualizationFEAmesh>(*(mesh.get()));
	mvisualizebeamA->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_ELEM_BEAM_MZ);
	mvisualizebeamA->SetColorscaleMinMax(-0.4, 0.4);
	mvisualizebeamA->SetSmoothFaces(true);
	mvisualizebeamA->SetWireframe(false);
	mesh->AddAsset(mvisualizebeamA);

	auto mvisualizebeamC = std::make_shared<ChVisualizationFEAmesh>(*(mesh.get()));
	mvisualizebeamC->SetFEMglyphType(ChVisualizationFEAmesh::E_GLYPH_NODE_DOT_POS); // E_GLYPH_NODE_CSYS
	mvisualizebeamC->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_NONE);
	mvisualizebeamC->SetSymbolsThickness(0.006);
	mvisualizebeamC->SetSymbolsScale(0.01);
	mvisualizebeamC->SetZbufferHide(false);
	mesh->AddAsset(mvisualizebeamC);
}
