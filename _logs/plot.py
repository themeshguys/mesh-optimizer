import sys
import re
import matplotlib.pyplot as plt

def parse_data(file_path):
    max_values = []
    values = []
    value_regex = "^Value ([-+]?[0-9]*\.?[0-9]+)$"
    generation_regex = "^#generationNo: [0-9]+$"

    with open(file_path) as input_file_stream: 
        for line in input_file_stream:
            generation_search = re.search(generation_regex, line)
            if generation_search:
                if len(values) != 0:
                    max_values.append(min(values))
                values.clear()
            else:
                value_search = re.search(value_regex, line)
                if value_search:
                    values.append(float(value_search.group(1)))
    return max_values

def plot(x, y):
    plt.plot(x, y)
    plt.show()

def main():
    values = parse_data(sys.argv[1])
    plot(range(len(values)), values)

if __name__ == "__main__":
    main()